package com.revolutDemo.repo

import com.revolutDemo.model.RatesResponse
import com.revolutDemo.repo.api.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateRepo
{
    companion object {
        private var INSTANCE: RateRepo? = null
        fun getInstance() = INSTANCE
            ?: RateRepo().also {
                INSTANCE = it
            }
    }

    fun getList(onResult: (isSuccess: Boolean, response: RatesResponse?) -> Unit)
    {
        ApiClient.instance.getRepo("EUR").enqueue(object : Callback<RatesResponse> {
            override fun onResponse(call: Call<RatesResponse>?, response: Response<RatesResponse>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<RatesResponse>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }
}