package com.revolutDemo.repo.api

import com.revolutDemo.model.RatesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    //https://hiring.revolut.codes/api/android/latest?base=EUR
    @GET("android/latest")
    fun getRepo(@Query("base") base: String = "EUR"): Call<RatesResponse>
}