package com.revolutDemo.ui.rates

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolutDemo.databinding.FragmentRatesBinding
import kotlinx.android.synthetic.main.fragment_rates.*
import org.jetbrains.anko.longToast

class RatesFragment : Fragment()
{
    private lateinit var viewDataBinding: FragmentRatesBinding
    private lateinit var adapter: RatesListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentRatesBinding.inflate(inflater, container, false).apply {
            val factory = RatesViewModelFactory(this@RatesFragment.context!!) { index ->
                adapter.moveToTop(index)
            }
            viewModel = ViewModelProvider(this@RatesFragment, factory).get(RatesViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.viewModel?.fetch()

        setupAdapter()
        setupObservers()
    }

    override fun onPause() {
        super.onPause()
        viewDataBinding.viewModel?.onPause()
    }

    override fun onResume() {
        super.onResume()
        viewDataBinding.viewModel?.onResume()
    }

    private fun setupObservers() {
        viewDataBinding.viewModel?.list?.observe(viewLifecycleOwner, Observer {
            adapter.updateRepoList(it)
        })

        viewDataBinding.viewModel?.toastMessage?.observe(viewLifecycleOwner, Observer {
            activity?.longToast(it)
        })
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewModel
        if (viewModel != null) {
            adapter = RatesListAdapter(viewDataBinding.viewModel!!)
            val layoutManager = LinearLayoutManager(activity)
            repo_list_rv.layoutManager = layoutManager
            repo_list_rv.addItemDecoration(DividerItemDecoration(activity, layoutManager.orientation))
            repo_list_rv.adapter = adapter
        }
    }
}