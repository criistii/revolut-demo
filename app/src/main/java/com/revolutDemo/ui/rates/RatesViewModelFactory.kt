package com.revolutDemo.ui.rates

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

// Custom VM factory. Has reference to Context (needed to load flags json)
class RatesViewModelFactory(private val context: Context, private val moveItemToTop: (Int) -> Unit): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = RatesViewModel(context, moveItemToTop) as T
}