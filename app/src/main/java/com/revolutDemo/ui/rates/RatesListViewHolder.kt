package com.revolutDemo.ui.rates

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.revolutDemo.BR
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_rates_list_item.view.*

class RatesListViewHolder constructor(
    private val dataBinding: ViewDataBinding,
    private val repoListViewModel: RatesViewModel)
    : RecyclerView.ViewHolder(dataBinding.root) {

    private val imageView: ImageView = itemView.imageView
    private val editText: EditText = itemView.editText

    private var itemData: RateItem? = null

    init
    {
        editText.setOnFocusChangeListener { _, isFocused ->
            repoListViewModel.focusedItem = if (isFocused) itemData else null

            if (!isFocused) {
                val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(editText.windowToken, 0)
            }
        }
    }

    fun setup(itemData: RateItem)
    {
        this.itemData = itemData

        dataBinding.setVariable(BR.itemData, itemData)
        dataBinding.executePendingBindings()

        if (!itemData.countryCode.isNullOrEmpty()) {
            val url = "https://www.countryflags.io/${itemData.countryCode!!.toLowerCase()}/flat/64.png"
            Picasso.get().load(url).into(imageView);
        }
        else
            imageView.setImageDrawable(null)
    }
}