package com.revolutDemo.ui.rates

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.revolutDemo.databinding.ViewRatesListItemBinding
import com.revolutDemo.BR
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_rates_list_item.view.*

class RatesListAdapter(private val repoListViewModel: RatesViewModel) : RecyclerView.Adapter<RatesListViewHolder>()
{
    var repoList: List<RateItem> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val dataBinding = ViewRatesListItemBinding.inflate(inflater, parent, false)
        return RatesListViewHolder(dataBinding, repoListViewModel)
    }

    override fun getItemCount() = repoList.size

    override fun onBindViewHolder(holder: RatesListViewHolder, position: Int) {
        holder.setup(repoList[position])
    }

    fun updateRepoList(repoList: List<RateItem>) {
        this.repoList = repoList
        notifyDataSetChanged()
    }

    fun moveToTop (index: Int)
    {
        notifyItemMoved(index, 0)
    }
}