package com.revolutDemo.ui.rates

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import com.revolutDemo.model.RatesResponse
import com.revolutDemo.repo.RateRepo
import com.revolutDemo.util.BaseViewModel
import com.revolutDemo.util.FlagHelper

class RatesViewModel(private val context: Context, private val moveItemToTop: (Int) -> Unit) : BaseViewModel()
{
    private val rateItems = mutableListOf<RateItem>()
    val list = MutableLiveData<List<RateItem>>(rateItems)

    var focusedItem: RateItem? = null
        set(value) {
            // Cleanup up a field when it is deselected
            if (field != null)
            {
                val doubleValue = field!!.textValue.toDoubleOrNull()
                field!!.textValue = formatCurrency(doubleValue)
            }

            field = value

            // If a new field is selected, set my currency. Otherwise keep the current one
            myCurrency = field?.currency ?: myCurrency

            if (value != null)
                moveToTop(value)
        }

    private var currency = "EUR"
    private var myCurrency = currency


    // Timer for refreshing data every one second
    // https://stackoverflow.com/questions/55570990/kotlin-call-a-function-every-second
    private var mainHandler: Handler = Handler(Looper.getMainLooper())
    private val updateTextTask = object : Runnable {
        override fun run() {
            fetch()
            mainHandler.postDelayed(this, 1000)
        }
    }

    fun fetch()
    {
        // Skip loop if data already loading
        if (isDataLoading.value == true)
            return

        isDataLoading.value = true

        RateRepo.getInstance().getList { isSuccess, response ->
            isDataLoading.value = false
            if (isSuccess)
            {
                updateLiveData(response)
                isEmpty.value = false
                hasData.value = true
            }
        }
    }

    override fun onPause() {
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onResume() {
        mainHandler.post(updateTextTask)
    }

    private  fun moveToTop(item: RateItem)
    {
        val index = rateItems.indexOf(item)
        rateItems.remove(item)
        rateItems.add(0, item)
        moveItemToTop(index)
    }

    private fun updateLiveData(response: RatesResponse?)
    {
        if (response != null)
        {
            // Create list from server data
            var newRateItems = response.rates.map { RateItem(this, it.key, it.value) }.toMutableList()

            // Add missing currency
            newRateItems.add(0, RateItem(this, currency, 1.0))

            // Set country code based on currency
            val ccMap = FlagHelper(context).getCurrencyToCountryMap()
            for (l in newRateItems) {
                if (ccMap.containsKey(l.currency))
                    l.countryCode = ccMap[l.currency]
            }

            // Update reference currency
            currency = response.baseCurrency

            if (rateItems.count() == 0) {
                rateItems.addAll(newRateItems)
            }
            else
            {
                // Update to new exchange rates ...
                for (item in rateItems)
                {
                    val correspondingItem = newRateItems.find { it.currency == item.currency } ?: continue
                    item.exchangeRate = correspondingItem.exchangeRate
                }
            }

            updateData()
        }
    }

    private fun updateData()
    {
        val refItem = rateItems.find { it.currency == myCurrency } ?: return

        for (item in rateItems)
        {
            if (item == focusedItem)
                continue

            val refValue = refItem.textValue.toDoubleOrNull()
            if (refValue != null)
            {
                val value = item.exchangeRate * refValue!! / refItem.exchangeRate
                item.textValue = formatCurrency(value)
            }
            else
            {
                item.textValue = ""
            }
        }
    }

    fun textValueDidChange(item: RateItem)
    {
        if (item == focusedItem)
            updateData()
        else
            item.notifyChange()
    }

    fun formatCurrency(value: Double?): String {
        val v = value ?: return ""
        return  "%.2f".format(v)
    }
}

class RateItem(private val parent: RatesViewModel, val currency: String, var exchangeRate: Double) : BaseObservable()
{
    var countryCode: String? = null

    @Bindable
    var textValue: String = parent.formatCurrency(exchangeRate)
        set(value) {
            field = value
            parent.textValueDidChange(this)
        }
}