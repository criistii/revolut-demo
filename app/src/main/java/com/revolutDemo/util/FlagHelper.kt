package com.revolutDemo.util

import android.content.Context
import com.google.gson.Gson
import com.revolutDemo.R

class FlagHelper (private val applicationContext: Context)
{
    fun getCurrencyToCountryMap(): Map<String, String>
    {
        val text = applicationContext.resources.openRawResource(R.raw.country_currency)
            .bufferedReader().use { it.readText() }

        val data = Gson().fromJson(text, Map::class.java)

        return data.entries.associate { (k, v) -> v as String to k as String }
    }
}