package com.revolutDemo.util

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()
{
    val isEmpty = MutableLiveData<Boolean>().apply { value = false }
    val hasData = MutableLiveData<Boolean>().apply { value = false }
    val isDataLoading = MutableLiveData<Boolean>().apply { value = false }
    val toastMessage = MutableLiveData<String>()

    open fun onPause() {}
    open fun onResume() {}
}