package com.revolutDemo.model

data class RatesResponse (
    val baseCurrency: String,
    val rates: Map<String, Double>
)